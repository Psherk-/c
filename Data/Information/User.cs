﻿using ArrrQ.Core.SocketAsync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Data.Information
{
    class User
    {
        private int _id, _rank, _room;
        private string _name, _sexo, _mision, _look, _connection;
        private string[] _money; //0 = credits, 1 = duckets

        public User(int id, string name, string sexo, string mision, string look, string connection, string money, int rank, int room)
        {
            _id = id;
            _name = name;
            _sexo = sexo;
            _mision = mision;
            _look = look;
            _connection = connection;
            _money = money.Split(';');
            _rank = rank;
            _room = room;
        }

        public int Id() { return _id; }
        public string Name() { return _name; }
        public string Mision() { return _mision; }
        public string Look() { return _look; }
        public string Connection() { return _connection; }
        public string[] Money() { return _money; }
    }
}
