﻿using ArrrQ.Core;
using ArrrQ.Core.MySQL;
using ArrrQ.Core.SocketAsync;
using ArrrQ.Data.Information;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Data.Manager
{
    class UserManager
    {
        private Dictionary<int, User> _sessions;
        public static bool byTicket(string v, SocketData user)
        {
            try
            {
                Statement execute = new Statement();
                execute.SQL("SELECT * FROM users WHERE ticket = ?1", SQLType.DataRow);
                execute.addParameter("1", v);
                DataRow dRow = execute.getData<DataRow>();
                if (dRow == null)
                    return false;

                user._session = new User(int.Parse(dRow["id"].ToString()), (string)dRow["username"], (string)dRow["sexo"], (string)dRow["mision"], (string)dRow["look"], (string)dRow["connection"], (string)dRow["money"], int.Parse(dRow["rank"].ToString()), int.Parse(dRow["room"].ToString()));
                MainCore.Logger().setString(user.Ip + " Connected!");
                execute.Finish();
                return true;

            }
            catch(Exception e)
            {
                MainCore.Logger().setError(e);
                return false;
            }
        }
    }
}
