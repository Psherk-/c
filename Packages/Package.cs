﻿using ArrrQ.Core;
using ArrrQ.Core.Utils;
using ArrrQ.Sulake.Incomings.Handshake;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Packages
{
    class Package
    {
        public JObject PacketId { get; private set; }
        public Dictionary<int, IPacket> _packetList { get; private set; }

        public Package()
        {
            try
            {
                ParseJson ids = new ParseJson();
                ids.Parse("PacketID");
                PacketId = ids.getData();
                ids.Dispose();
            } 
            catch(Exception e)
            {
                MainCore.Logger().setError(e);
            }

            _packetList = new Dictionary<int, IPacket>();
            PackageList();
        }

        private void PackageList()
        {
            /*Handshake*/
            _packetList.Add((int)PacketId["Incoming"]["Production"], new Production());
            _packetList.Add((int)PacketId["Incoming"]["SendBanner"], new SendBanner());
            _packetList.Add((int)PacketId["Incoming"]["SecretKey"], new SecretKey());
        }

        public bool CheckIncoming(int x)
        {
            if (_packetList.ContainsKey(x))
                return true;
            else
                return false;
        }
    }
}
