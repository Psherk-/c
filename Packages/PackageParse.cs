﻿using ArrrQ.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Packages
{
    /*Thanks to Itachi*/
    class PackageParse
    {
        private readonly int _header = 0;
        private List<byte> _Message;

        public PackageParse()
        {
            _Message = new List<byte>();
        }
        public PackageParse(int x)
        {
            _Message = new List<byte>();
            _header = x;
        }
        public void addShort(int x)
        {
            Int16 s = (Int16)x;
            addByte(BitConverter.GetBytes(s), true);
        }
        public void addInt(int x)
        {
            addByte(BitConverter.GetBytes(x), true);
        }
        public void addString(string x)
        {
            addShort(x.Length);
            addByte(Encoding.ASCII.GetBytes(x), false);
        }
        public void addBool(bool x)
        {
            addByte(new byte[] { (byte)(x ? 1 : 0) }, false);
        }
        public void addByte(byte[] x, bool IsInt)
        {
            if (IsInt)
            {
                for (int i = (x.Length - 1); i > -1; i--)
                    _Message.Add(x[i]);
            }
            else
                _Message.AddRange(x);
        }
        public byte[] ToBytes()
        {
            List<byte> Final = new List<byte>();
            Final.AddRange(BitConverter.GetBytes(_Message.Count)); // packet len
            Final.Reverse();
            Final.AddRange(_Message); // Add Packet

            if(MainCore.Composers)
                MainCore.Logger().setPacket("[Composer {Send} => " + _header + "/" + Final.ToArray().Length + "] " + PackageEncryption.decypherPacket(Final.ToArray()));
            return Final.ToArray();
        }
    }
}
