﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Packages
{
    /*Thanks to LittleJ*/
    class PackageEncryption
    {
        public static int decypherInt(byte[] v)
        {

            if ((v[0] | v[1] | v[2] | v[3]) < 0)

                return -1;

            return ((v[0] << 24) + (v[1] << 16) + (v[2] << 8) + (v[3] << 0));

        }

        public static int decypherShort(byte[] v)
        {

            if ((v[0] | v[1]) < 0)

                return -1;

            return ((v[0] << 8) + (v[1] << 0));

        }

        public static string decypherPacket(byte[] v)
        {
            return Encoding.Default.GetString(v).Replace(Convert.ToChar(0).ToString(), "[0]");
        }
    }
}
