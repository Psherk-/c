﻿using ArrrQ.Core;
using ArrrQ.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Sulake
{
    class Game
    {
        private Package _package;
        public Game()
        {
            try
            {
                _package = new Package();
                MainCore.Logger().setString("Incoming/PackageList {" + _package._packetList.Count + "}");
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
            }
        }

        public Package getPackage()
        {
            return _package;
        }
    }
}
