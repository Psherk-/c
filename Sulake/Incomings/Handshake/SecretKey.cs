﻿using ArrrQ.Core;
using ArrrQ.Core.SocketAsync;
using ArrrQ.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Sulake.Incomings.Handshake
{
    class SecretKey : IPacket
    {
        public void Init(SocketData user)
        {
            PackageParse parse = new PackageParse((int)MainCore.getGame().getPackage().PacketId["Composer"]["SecretKey"]);
            string cipherPublicKey = user.readString();
            BigInteger sharedKey = MainCore._habboCrypto.CalculateDiffieHellmanSharedKey(cipherPublicKey);
            if (sharedKey != 0)
            {
                parse.addString(MainCore._habboCrypto.GetRSADiffieHellmanPublicKey());
                parse.addBool(false);

                //RC4
            }
            else
            {
                MainCore.Logger().setString("RC4 Error");
            }
            user.Send(parse);
        }
    }
}
