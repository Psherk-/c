﻿using ArrrQ.Core;
using ArrrQ.Core.SocketAsync;
using ArrrQ.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Sulake.Incomings.Handshake
{
    class SendBanner : IPacket
    {
        public void Init(SocketData user)
        {
            PackageParse parse = new PackageParse((int)MainCore.getGame().getPackage().PacketId["Composer"]["SendBanner"]);
            parse.addString(MainCore._habboCrypto.GetRSADiffieHellmanPKey());
            parse.addString(MainCore._habboCrypto.GetRSADiffieHellmanGKey());
            user.Send(parse);
        }
    }
}
