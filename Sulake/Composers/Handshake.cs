﻿using ArrrQ.Core;
using ArrrQ.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Sulake.Composers
{
    class Handshake
    {
        public static PackageParse Banner()
        {
            PackageParse parse = new PackageParse((int)MainCore.getGame().getPackage().PacketId["Composer"]["SendBanner"]);
            parse.addString("");
            parse.addString("");
            return parse;
        }
    }
}
