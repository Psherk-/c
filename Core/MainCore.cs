﻿using ArrrQ.Core.Forms;
using ArrrQ.Core.MySQL;
using ArrrQ.Core.SocketAsync;
using ArrrQ.Core.Utils;
using ArrrQ.Packages;
using ArrrQ.Sulake;
using HEncryption;
using HEncryption.Security.Cryptography;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core
{
    class MainCore
    {
        private static Logger _Log;
        public static JObject getConfiguration { get; private set; }
        private static Connection _mysql;
        private static SocketCreate _socket;
        private static Game _game;
        public static bool Incomings = true, Composers = true;



        private static BigInteger _modules = BigInteger.Parse("0086851DD364D5C5CECE3C883171CC6DDC5760779B992482BD1E20DD296888DF91B33B936A7B93F06D29E8870F703A216257DEC7C81DE0058FEA4CC5116F75E6EFC4E9113513E45357DC3FD43D4EFAB5963EF178B78BD61E81A14C603B24C8BCCE0A12230B320045498EDC29282FF0603BC7B7DAE8FC1B05B52B2F301A9DC783B7", System.Globalization.NumberStyles.HexNumber);
        private static BigInteger _d = BigInteger.Parse("0059AE13E243392E89DED305764BDD9E92E4EAFA67BB6DAC7E1415E8C645B0950BCCD26246FD0D4AF37145AF5FA026C0EC3A94853013EAAE5FF1888360F4F9449EE023762EC195DFF3F30CA0B08B8C947E3859877B5D7DCED5C8715C58B53740B84E11FBC71349A27C31745FCEFEEEA57CFF291099205E230E0C7C27E8E1C0512B", System.Globalization.NumberStyles.HexNumber);
        private static BigInteger _exponent = 3;
        public static HabboEncryption _habboCrypto { get; private set; }

        public static bool Init(main v)
        {
            try
            {
                _Log = new Logger(v);
                #region ConfigurationFile
                ParseJson config = new ParseJson();
                config.Parse("configData");
                getConfiguration = config.getData();
                config.Dispose();
                #endregion
                _mysql = new Connection();
                _socket = new SocketCreate();
                _game = new Game();
                _Log.setString("Ready!");

                /*Thanks to Joopie*/
                RSACParameters rsaParameters = new RSACParameters(_d, _modules, _exponent);
                _habboCrypto = new HabboEncryption(rsaParameters, 128);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Logger Logger()
        {
            return _Log;
        }
        public static Connection getConnection()
        {
            return _mysql;
        }
        public static Game getGame()
        {
            return _game;
        }

        public static void Shutdown()
        {
            _mysql.Dispose();
            _socket.Dispose();
            getConfiguration = null;
        }
    }
}
