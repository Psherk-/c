﻿using ArrrQ.Data.Information;
using ArrrQ.Packages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.SocketAsync
{
    class SocketData
    {
        private readonly Socket _socket;
        private byte[] _buffer = new byte[1024];
        public BinaryReader readPacket;
        public string Ip { get; private set; }
        public User _session = null;

        public SocketData(Socket x)
        {
            _socket = x;
            Ip = _socket.RemoteEndPoint.ToString().Split(':')[0];
            ReceiveData();
        }

        private void ReceiveData()
        {
            try
            {
                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ReadCallback, _socket);
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
                Disconnect();
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                int bytesRead = _socket.EndReceive(ar);
                if (bytesRead > 0 && _socket != null && _socket.Available == 0)
                {
                    byte[] x = new byte[bytesRead];
                    Array.Copy(_buffer, x, bytesRead);
                    ProcessReceive(x);
                }
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
                Disconnect();
            }
            finally
            {
                this.ReceiveData();
            }
        }

        private void ProcessReceive(byte[] x)
        {
            if(x[0] == 60)
            {
                Send("<?xml version=\"1.0\"?>\r\n" +
                       "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n" +
                       "<cross-domain-policy>\r\n" +
                       "<allow-access-from domain=\"*\" to-ports=\"1-31111\" />\r\n" +
                       "</cross-domain-policy>\x0");
            }
            else
            {
                int b = 0;
                while(b < x.Length)
                {
                    int Lenght = PackageEncryption.decypherInt(new byte[] {x[b++], x[b++], x[b++], x[b++]});
                    int Header = PackageEncryption.decypherShort(new byte[] {x[b++], x[b++]});
                    byte[] Content = new byte[Lenght - 2];
                    for (int i = 0; i < Content.Length && b < x.Length; i++)
                    {
                        Content[i] = x[b++];
                    }
                    using (BinaryReader PacketRead = new BinaryReader(new MemoryStream(Content)))
                    {
                        readPacket = PacketRead;
                        if (MainCore.getGame().getPackage().CheckIncoming(Header)) //Existe
                        {
                            if (MainCore.Incomings)
                                MainCore.Logger().setPacket("[Incomings {Handled} => " + Header + "/" + Lenght + "] " + PackageEncryption.decypherPacket(Content));
                            IPacket Invoke = MainCore.getGame().getPackage()._packetList[Header];
                            Invoke.Init(this);
                        }
                        else
                        {
                            if (MainCore.Incomings)
                                MainCore.Logger().setPacket("[Incomings {Unhandled} => " + Header + "/" + Lenght + "] " + PackageEncryption.decypherPacket(Content));
                        }
                    }                    
                }
            }
        }

        #region Send
        private void SendFinally(IAsyncResult ar)
        {
            try
            {
                if (_socket == null)
                    return;
                _socket.EndSend(ar);
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
            }
        }
        public void Send(byte[] v)
        {
            try
            {
                _socket.BeginSend(v, 0, v.Length, SocketFlags.None, new AsyncCallback(SendFinally), _socket);
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
            }
        }
        public void Send(string v)
        {
            Send(Encoding.ASCII.GetBytes(v));
        }
        public void Send(PackageParse v)
        {
            Send(v.ToBytes());
        }
        #endregion

        #region Getters
        public int readInt16()
        {
            return BitConverter.ToInt16(readPacket.ReadBytes(2), 0);;
        }
        public int readInt()
        {
            try
            {
                return BitConverter.ToInt32(readPacket.ReadBytes(4), 0);
            }
            catch
            {
                return 0;
            }
        }
        public string readString()
        {
            try
            {
                return Encoding.Default.GetString(readPacket.ReadBytes(PackageEncryption.decypherShort(readPacket.ReadBytes(2))));
            }
            catch
            {
                return "";
            }
        }
        public bool readBool()
        {
            return readInt() > 0;
        }
        public byte[] readByte()
        {
            return new byte[1]; //
        }
        #endregion

        public void Disconnect()
        {
            MainCore.Logger().setString(_socket.RemoteEndPoint.ToString().Split(':')[0] + " Disconnect!");
            if (_socket == null)
                return;

            _session = null;
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }
    }
}
