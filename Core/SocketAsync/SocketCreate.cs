﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.SocketAsync
{
    class SocketCreate : IDisposable
    {
        private Socket _socket;
        public SocketCreate()
        {
            MainCore.Logger().setString("Starting SocketAsync...");
            try
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Bind(new IPEndPoint(IPAddress.Parse(MainCore.getConfiguration["Socket"]["host"].ToString()), (int)MainCore.getConfiguration["Socket"]["port"]));
                _socket.Listen((int)MainCore.getConfiguration["Socket"]["limited"]);
                MainCore.Logger().setString("SocketAsync started!");
                _socket.BeginAccept(new AsyncCallback(AcceptCallback), _socket);
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
                throw;
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                Socket v = ((Socket)ar.AsyncState).EndAccept(ar);
                new SocketData(v);
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
            }
            finally
            {
                try
                {
                    _socket.BeginAccept(new AsyncCallback(AcceptCallback), _socket);
                }
                catch(Exception e)
                {
                    MainCore.Logger().setError(e);
                }
            }
        }

        public void Dispose()
        {
            _socket.Close();
        }
    }
}
