﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArrrQ.Core.Forms
{
    public partial class main : Form
    {
        private static int hours, minutes, seconds = 0;
        private static bool InitConfig = false;
        public main()
        {
            InitializeComponent();
            this.Text = Program.Name +" Emulator";
            String v = Program.Name + " Emulator [Versión " + Program.Version + "]\n";
            v += "Copyright <c> 2016\n";
            v += "Developed by " + Program.Author + "\n\n";
            richTextBox1.AppendText(v);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            seconds++;
            if (seconds == 60)
            {
                minutes++;
                seconds = 0;
            }
            else if (minutes == 60)
            {
                hours++;
                minutes = 0;
            }

            label1.Text = hours.ToString().PadLeft(2, '0') + ":" + minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text.ToString() == "Start")
            {
                if(MainCore.Init(this))
                {
                    timer1.Enabled = true;
                    button1.Text = "Shutdown";
                }
            }
            else
            {
                timer1.Enabled = false;
                MainCore.Shutdown();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //Incomings
            MainCore.Incomings = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            //Composers
            MainCore.Composers = checkBox2.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(!InitConfig)
            {
                form2 Invoke = new form2();
                Invoke.Show();
            }
        }
    }
}
