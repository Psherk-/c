﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.MySQL
{
    class Connection : IDisposable
    {
        public MySqlConnection _connection {get; private set;}
        public string ConnectionString { get; private set; }

        public Connection()
        {
            MainCore.Logger().setString("Starting MySQL connection...");
            try
            {
                MySqlConnectionStringBuilder connect = new MySqlConnectionStringBuilder();
                connect.Server = (string)MainCore.getConfiguration["MySQL"]["host"];
                connect.Port = (uint)MainCore.getConfiguration["MySQL"]["port"];
                connect.UserID = (string)MainCore.getConfiguration["MySQL"]["username"];
                connect.Password = (string)MainCore.getConfiguration["MySQL"]["password"];
                connect.Database = (string)MainCore.getConfiguration["MySQL"]["database"];
                connect.Pooling = true;
                connect.MinimumPoolSize = 5;
                connect.MaximumPoolSize = 100;
                _connection = new MySqlConnection(ConnectionString = connect.ConnectionString);
                _connection.Open();
                MainCore.Logger().setString("Connection MySQL started!");
            }
            catch (Exception e)
            {
                MainCore.Logger().setError(e);
                throw;
            }
        }

        public void Dispose()
        {
            if (_connection == null)
                return;
            _connection.Close();
        }
    }
}
