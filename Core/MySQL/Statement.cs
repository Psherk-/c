﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.MySQL
{
    public class Statement
    {
        private string _sql;
        public object _send;
        private SQLType _type;
        private List<MySqlParameter> _parameters;

        public void SQL(string sql, SQLType x)
        {
            if (sql == "")
                return;

            _sql = sql;
            _type = x;
            _parameters = new List<MySqlParameter>();
        }

        public void addParameter(string x, object v)
        {
            _parameters.Add(new MySqlParameter(string.Format("?{0}", x), v));
        }

        private void Type()
        {
            try
            {
                switch(_type)
                {
                    case SQLType.String:
                    _send = MySqlHelper.ExecuteNonQuery(MainCore.getConnection().ConnectionString, _sql, _parameters.ToArray());
                    break;
                    case SQLType.DataRow:
                    _send = MySqlHelper.ExecuteDataRow(MainCore.getConnection().ConnectionString, _sql, _parameters.ToArray());
                    break;

                    case SQLType.DataTable:
                    _send = MySqlHelper.ExecuteDataset(MainCore.getConnection().ConnectionString, _sql, _parameters.ToArray()).Tables[0];
                    break;
                }
            }
            catch
            {
                _send = null;
            }
        }

        public T getData<T>()
        {
            this.Type();
            return (T)_send;
        }
        
        public void Finish()
        {
            _parameters.Clear();
            _sql = null;
            _send = null;
        }
    }

    public enum SQLType
    {
        String,
        DataRow,
        DataTable
    }
}
