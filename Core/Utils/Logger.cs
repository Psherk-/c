﻿using ArrrQ.Core.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.Utils
{
    class Logger
    {
        private static main _form;
        delegate void _Message(string v, string x);
        public Logger(main v)
        {
            _form = v;
        }
        private void addMessage(string v, string x)
        {
            switch (v)
            {
                case "String":
                    var Time = DateTime.Now.ToString("HH:mm:ss");
                    _form.richTextBox1.SelectionColor = _form.richTextBox1.ForeColor;
                    _form.richTextBox1.AppendText("\n[" + Time + "] => " + x);
                    break;
                case "Int":
                    Time = DateTime.Now.ToString("HH:mm:ss");
                    _form.richTextBox1.SelectionColor = _form.richTextBox1.ForeColor;
                    _form.richTextBox1.AppendText("\n[" + Time + "] => " + x);
                    break;
                case "Packet":
                    Time = DateTime.Now.ToString("HH:mm:ss");
                    _form.richTextBox1.SelectionColor = Color.LightSteelBlue;
                    _form.richTextBox1.AppendText("\n"+x);
                    break;
                case "Error":
                    Time = DateTime.Now.ToString("HH:mm:ss");
                    _form.richTextBox1.SelectionColor = Color.Salmon;
                    _form.richTextBox1.AppendText("\n[" + Time + "][Error] => " + x);
                    break;
            }
        }
        public void setString(string x)
        {
            _Message s = new _Message(addMessage);
            _form.Invoke(s, new object[] { "String", x });
        }
        public void setInt(int x) {
            _Message s = new _Message(addMessage);
            _form.Invoke(s, new object[] { "Int", x });
        }
        public void setInt(string x) {
            setInt(int.Parse(x));
        }
        public void setPacket(string x)
        {
            _Message s = new _Message(addMessage);
            _form.Invoke(s, new object[] { "Packet", x });
        }
        public void setError(Exception x) {
            _Message s = new _Message(addMessage);
            _form.Invoke(s, new object[] { "Error", x.ToString() });

            StreamWriter WriteReportFile = File.AppendText(@"Exceptions.txt");
            WriteReportFile.Write("[" + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + "] " + x.ToString() + "\n\r\n\r");
            WriteReportFile.Close();
        }
    }
}
