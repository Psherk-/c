﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrrQ.Core.Utils
{
    public class ParseJson
    {
        private JObject _data;

        public void Parse(string file)
        {
            try
            {
                StreamReader x = new StreamReader("Configuration/"+ file +".json");
                _data = JObject.Parse(x.ReadToEnd());
                x.Close();
            }
            catch(Exception e)
            {
                MainCore.Logger().setError(e);
            }
        }

        public JObject getData()
        {
            return _data;
        }

        public void Dispose()
        {
            _data = null;
        }
    }
}
